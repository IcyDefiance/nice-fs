﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace nIceFS
{
    static class Volume
    {
        public const long SECTOR_SIZE = 512, MIN_SECTORS = 4;
        private const long FILE_INFO_SIZE = 25;
        private static BinaryReader read;
        private static BinaryWriter write;

        public static void CommandLoop(string volumePath)
        {
            using (FileStream fs = new FileStream(volumePath, FileMode.Open, FileAccess.ReadWrite))
            using (read = new BinaryReader(fs))
            using (write = new BinaryWriter(fs, Encoding.UTF8))
            {
                bool endLoop = false;
                while (!endLoop)
                {
                    Console.Write(Environment.NewLine + "#/: ");
                    string[] command = Console.ReadLine().Split(' ');
                    switch (command[0].ToLower())
                    {
                        case "help":
                            if (command.Length != 1)
                                Console.WriteLine("Invalid syntax: help");
                            else
                                Console.WriteLine("Available commands: unmount, create, catalog, info, write, read, delete");
                            break;
                        case "unmount":
                            if (command.Length != 1)
                                Console.WriteLine("Invalid syntax: unmount");
                            else
                                endLoop = true;
                            break;
                        case "create":
                            if (command.Length != 2)
                                Console.WriteLine("Invalid syntax: create (filename)" + Environment.NewLine + "Note spaces in file names are not supported yet.");
                            else if (!Create(command[1]))
                                Console.WriteLine("Error: Could not create file.");
                            break;
                        case "catalog":
                            if (command.Length != 1)
                                Console.WriteLine("Invalid syntax: catalog");
                            else
                                Catalog();
                            break;
                        case "info":
                            if (command.Length < 1 || command.Length > 2)
                                Console.WriteLine("Invalid syntax: info [filename]");
                            else if (command.Length == 1)
                                Info();
                            else
                                if (!Info(command[1]))
                                    Console.WriteLine("Could not display info on that file.");
                            break;
                        case "write":
                            if (command.Length != 4)
                                Console.WriteLine("Invalid syntax: write (filename) offset data");
                            else
                            {
                                long offset = 0;
                                if (Int64.TryParse(command[2], out offset))
                                {
                                    if (!Write(command[1], offset, Encoding.ASCII.GetBytes(command[3])))
                                        Console.WriteLine("Error: Could not write to file");
                                }
                                else
                                {
                                    Console.WriteLine("Invalid syntax: Offset must be a number");
                                }
                            }
                            break;
                        case "read":
                            if (command.Length < 2 || command.Length > 4)
                                Console.WriteLine("Invalid syntax: read (filename) [start] [end]");
                            else
                            {
                                long start = 0, end = 0;
                                if (command.Length > 2 && !Int64.TryParse(command[2], out start) || command.Length > 3 && !Int64.TryParse(command[3], out end))
                                    Console.WriteLine("Start and end, if included, must be numbers.");
                                else
                                {
                                    string result = Read(command[1], start, end);
                                    Console.WriteLine(result.Length > 0 ? result : "Could not find file.");
                                }
                            }
                            break;
                        case "delete":
                            if (command.Length != 2)
                                Console.WriteLine("Invalid syntax: delete (filename)");
                            else if (!Delete(command[1]))
                                Console.WriteLine("Error: Could not delete file.");
                            break;
                        case "truncate":
                            if (command.Length != 2)
                                Console.WriteLine("Invalid syntax: truncate (filename)");
                            else if (!Truncate(command[1]))
                                Console.WriteLine("Error: Could not truncate file.");
                            break;
                        case "set":
                            command[3] = command[3].ToLower();
                            if (command.Length != 4 || command[2] != "readonly" || (command[3] != "true" && command[3] != "false"))
                            {
                                Console.WriteLine("Invalid syntax: set (filename) {readonly} {true,false}");
                            }
                            else if (command[3] == "true")
                            {
                                if (!Set(command[1], SetAttribs.ReadOnly, true))
                                    Console.WriteLine("Error: Could not set attribute.");
                            }
                            else if (command[3] == "false")
                            {
                                if (!Set(command[1], SetAttribs.ReadOnly, false))
                                    Console.WriteLine("Error: Could not set attribute.");
                            }
                            break;
                        default:
                            Console.WriteLine("Error: Command " + command[0] + " not recognized. Type unmount to exit.");
                            break;
                    }
                }
            }
        }

        private static bool Create(string filePath)
        {
            string[] pathSplit = filePath.Split(new char[] { '/', '\\' });
            string fileName = pathSplit[pathSplit.Length - 1];

            //validate file name
            if (pathSplit.Length > 1)
            {
                Console.WriteLine("Error: Directories not supported yet.");
                return false;
            }
            else if (fileName.Length == 0)
            {
                Console.WriteLine("Error: Must provide a file name.");
                return false;
            }
            else if (fileName.Length > 127)
            {
                Console.WriteLine("File name must be <= 127 characters.");
                return false;
            }

            //find open spot for file in catalog
            long infoStart = SECTOR_SIZE + 9, openInfoPos = 0;
            while (openInfoPos == 0)
            {
                for (long i = infoStart; i < infoStart + SECTOR_SIZE - 17 && openInfoPos == 0; i += FILE_INFO_SIZE)
                {
                    read.BaseStream.Seek(i, SeekOrigin.Begin);
                    long dataSector = read.ReadInt64();
                    if (dataSector == 0)
                        openInfoPos = i;
                }

                read.BaseStream.Seek(infoStart + SECTOR_SIZE - 17, SeekOrigin.Begin);
                infoStart = read.ReadInt64() * SECTOR_SIZE + 9;
                if (openInfoPos == 0 && infoStart == 9)
                {
                    long nextSector = FindOpenSectorIndex();
                    if (nextSector == 0)
                        return false;

                    read.BaseStream.Seek(infoStart + SECTOR_SIZE - 17, SeekOrigin.Begin);
                    write.Write(nextSector);

                    infoStart = nextSector * SECTOR_SIZE + 9;
                }
            }

            //find open sector for file data
            long openSectorIndex = FindOpenSectorIndex();
            if (openSectorIndex == 0)
                return false;
            else
                ClearSector(openSectorIndex);

            //find open slot for file name
            read.BaseStream.Seek(SECTOR_SIZE + 1, SeekOrigin.Begin);
            long nameStart = read.ReadInt64() * SECTOR_SIZE + 1, curNamePos = nameStart, openNamePos = 0;
            int nameLength = Encoding.UTF8.GetByteCount(filePath) + 1;
            read.BaseStream.Seek(nameStart, SeekOrigin.Begin);
            while (openNamePos == 0)
            {
                while (read.ReadByte() != 0)
                {
                    curNamePos++;
                    if (curNamePos >= nameStart + SECTOR_SIZE - 8)
                        return false;
                }

                bool open = true;
                for (int i = 0; i < nameLength - 1 && open; i++)
                    if (read.ReadByte() != 0)
                        open = false;

                if (open)
                    openNamePos = curNamePos;
            }

            //write file info
            write.BaseStream.Seek(openInfoPos, SeekOrigin.Begin);
            write.Write(openSectorIndex);
            write.Write(openNamePos);
            write.Write(false);
            DateTime now = DateTime.Now;
            write.Write(VolumeMath.DateTimeToInt(now));
            write.Write(VolumeMath.DateTimeToInt(now));

            //write file name
            write.BaseStream.Seek(openNamePos, SeekOrigin.Begin);
            write.Write(filePath);

            //reserve sector for file data
            write.BaseStream.Seek(openSectorIndex * SECTOR_SIZE, SeekOrigin.Begin);
            write.Write((byte)0xFF);

            return true;
        }

        private static void Catalog()
        {
            string fileList = "";
            string delimiter = ", ";

            long start = SECTOR_SIZE + 9;
            for (long i = start; i < start + SECTOR_SIZE - 17; i += FILE_INFO_SIZE)
            {
                read.BaseStream.Seek(i, SeekOrigin.Begin);
                if (read.ReadInt64() != 0)
                {
                    long fileNamePos = read.ReadInt64();
                    read.BaseStream.Seek(fileNamePos, SeekOrigin.Begin);
                    fileList += read.ReadString() + delimiter;
                }
            }

            if (fileList.Length > 2)
                Console.WriteLine(fileList.Substring(0, fileList.Length - delimiter.Length));
            else
                Console.WriteLine("No files found.");
        }

        private static void Info()
        {
            long size = read.BaseStream.Length, freeSpace = size - SECTOR_SIZE, fileCount = 0;

            read.BaseStream.Seek(SECTOR_SIZE, SeekOrigin.Begin);
            for (long i = SECTOR_SIZE; i < size; i += SECTOR_SIZE)
            {
                read.BaseStream.Seek(i, SeekOrigin.Begin);
                if (read.ReadByte() == 0xFF)
                    freeSpace -= SECTOR_SIZE;
            }

            long start = SECTOR_SIZE + 9;
            for (long i = start; i < start + SECTOR_SIZE - 17; i += FILE_INFO_SIZE)
            {
                read.BaseStream.Seek(i, SeekOrigin.Begin);
                if (read.ReadInt64() != 0)
                    fileCount++;
            }

            Console.WriteLine("Size: " + size);
            Console.WriteLine("Free space: " + freeSpace);
            Console.WriteLine("File count: " + fileCount);
        }

        private static bool Info(string fileName)
        {
            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null)
                return false;

            long size = 0;
            bool eof = false;
            read.BaseStream.Seek(fileInfo.DataSectorIndex * SECTOR_SIZE + SECTOR_SIZE - 8, SeekOrigin.Begin);
            while (!eof)
            {
                long nextSector = read.ReadInt64();
                if (nextSector == 0)
                    eof = true;
                else
                    read.BaseStream.Seek(nextSector * SECTOR_SIZE + SECTOR_SIZE - 8, SeekOrigin.Begin);

                size += SECTOR_SIZE - 9;
            }

            Console.WriteLine("Size: " + size + " bytes");
            Console.WriteLine("Read only: " + fileInfo.ReadOnly);
            Console.WriteLine("Time created: " + fileInfo.Created.ToString("d") + " at " + fileInfo.Created.ToString("t"));
            Console.WriteLine("Time modified: " + fileInfo.Created.ToString("d") + " at " + fileInfo.Created.ToString("t"));
            return true;
        }

        private static bool Write(string fileName, long offset, byte[] data)
        {
            if (offset < 0)
                return false;

            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null || fileInfo.ReadOnly)
                return false;

            //find sectorOffset and reduce byteOffset accordingly
            long sectorOffset = 0, byteOffset = offset;
            while ((byteOffset + 9) / SECTOR_SIZE > 0)
            {
                byteOffset -= SECTOR_SIZE - 9;
                sectorOffset++;
            }

            //find the first sector to write to
            long writeSectorIndex = fileInfo.DataSectorIndex;
            for (int i = 0; i < sectorOffset; i++)
            {
                write.BaseStream.Seek(writeSectorIndex * SECTOR_SIZE + SECTOR_SIZE - 8, SeekOrigin.Begin);
                writeSectorIndex = read.ReadInt64();
                if (writeSectorIndex == 0)
                {
                    writeSectorIndex = FindOpenSectorIndex();
                    if (writeSectorIndex == 0)
                        return false;

                    write.BaseStream.Seek(-9, SeekOrigin.Current);
                    write.Write(writeSectorIndex);
                    write.BaseStream.Seek(writeSectorIndex * SECTOR_SIZE, SeekOrigin.Begin);
                    write.Write((byte)0xFF);
                    ClearSector(writeSectorIndex);
                }
            }

            //split the data into chunks based on sector boundaries. hopefully this doesn't have bugs, because the second I finished typing it I forgot how it works.
            List<List<byte>> dataSplit = new List<List<byte>>();
            dataSplit.Add(data.ToList());
            if (byteOffset < SECTOR_SIZE - 9 && byteOffset + dataSplit[0].Count > SECTOR_SIZE - 9)
            {
                dataSplit.Add(dataSplit[0].GetRange((int)(SECTOR_SIZE - byteOffset - 9), (int)(dataSplit[0].Count - (SECTOR_SIZE - byteOffset - 9))));
                dataSplit[0].RemoveRange((int)(SECTOR_SIZE - byteOffset - 9), (int)(dataSplit[0].Count - (SECTOR_SIZE - byteOffset - 9)));
            }
            while (dataSplit[dataSplit.Count - 1].Count > SECTOR_SIZE - 9)
            {
                dataSplit.Add(dataSplit[dataSplit.Count - 1].GetRange((int)(SECTOR_SIZE - 9), (int)(dataSplit[dataSplit.Count - 1].Count - (SECTOR_SIZE - 9))));
                dataSplit[dataSplit.Count - 2].RemoveRange((int)(SECTOR_SIZE - 9), (int)(dataSplit[dataSplit.Count - 1].Count - (SECTOR_SIZE - 9)));
            }

            //write things
            write.BaseStream.Seek(writeSectorIndex * SECTOR_SIZE + 1 + byteOffset, SeekOrigin.Begin);
            write.Write(dataSplit[0].ToArray());
            for (int i = 1; i < dataSplit.Count; i++)
            {
                read.BaseStream.Seek(SECTOR_SIZE - read.BaseStream.Position % SECTOR_SIZE - 8, SeekOrigin.Current);
                long nextSectorIndex = read.ReadInt64();
                if (nextSectorIndex == 0)
                {
                    nextSectorIndex = FindOpenSectorIndex();
                    if (nextSectorIndex == 0)
                        return false;

                    write.BaseStream.Seek(-8, SeekOrigin.Current);
                    write.Write(nextSectorIndex);
                    write.BaseStream.Seek(nextSectorIndex * SECTOR_SIZE, SeekOrigin.Begin);
                    write.Write((byte)0xFF);
                }

                write.BaseStream.Seek(nextSectorIndex * SECTOR_SIZE + 1, SeekOrigin.Begin);
                write.Write(dataSplit[i].ToArray());
            }

            return true;
        }

        private static string Read(string fileName, long start = 0, long end = 0)
        {
            string result = "";
            long length = end - start;

            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null)
                return "";

            read.BaseStream.Seek(fileInfo.DataSectorIndex * SECTOR_SIZE + 1, SeekOrigin.Begin);
            bool eof = false;
            while (!eof)
            {
                long offset = Math.Min(SECTOR_SIZE - 9, start);
                read.BaseStream.Seek(offset, SeekOrigin.Current);
                start -= offset;

                result += Encoding.ASCII.GetString(read.ReadBytes((int)(SECTOR_SIZE - 9 - offset)));
                long nextSectorIndex = read.ReadInt64();
                if (length > 0 && result.Length > length)
                {
                    result = result.Substring(0, (int)length);
                    eof = true;
                }
                else if (nextSectorIndex == 0)
                {
                    eof = true;
                }
                else
                {
                    read.BaseStream.Seek(nextSectorIndex * SECTOR_SIZE + 1, SeekOrigin.Begin);
                }
            }

            return result;
        }

        private static bool Delete(string fileName)
        {
            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null || fileInfo.ReadOnly)
                return false;

            write.BaseStream.Seek(fileInfo.FileInfoPosition, SeekOrigin.Begin);
            write.Write((long)0);   //delete from catalog

            write.BaseStream.Seek(read.ReadInt64(), SeekOrigin.Begin);
            int nameLength = Encoding.UTF8.GetByteCount(fileInfo.Name) + 1;
            for (int i = 0; i < nameLength; i++)
                write.Write((byte)0);   //delete name

            bool eof = false;
            write.BaseStream.Seek(fileInfo.DataSectorIndex * SECTOR_SIZE, SeekOrigin.Begin);
            while (!eof)
            {
                write.Write((byte)0);   //set data sectors to open again
                write.BaseStream.Seek(SECTOR_SIZE - 9, SeekOrigin.Current);
                long nextSector = read.ReadInt64();
                if (nextSector == 0)
                    eof = true;
                else
                    write.BaseStream.Seek(nextSector * SECTOR_SIZE, SeekOrigin.Begin);
            }

            return true;
        }

        private static bool Truncate(string fileName)
        {
            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null || fileInfo.ReadOnly)
                return false;

            Delete(fileName);
            Create(fileName);
            return true;
        }

        private static bool Set(string fileName, SetAttribs attrib, bool value)
        {
            FileInfo fileInfo = GetFileInfo(fileName);
            if (fileInfo == null)
                return false;

            byte offset;
            switch (attrib)
            {
                case SetAttribs.ReadOnly:
                    offset = 0;
                    break;
                default:
                    return false;
            }

            read.BaseStream.Seek(fileInfo.FileInfoPosition + 16, SeekOrigin.Begin);
            byte attribByte = read.ReadByte();
            if (value)
                attribByte |= (byte)(1 << offset);
            else
                attribByte &= (byte)(~(1 << offset));
            write.BaseStream.Seek(-1, SeekOrigin.Current);
            write.Write(attribByte);

            return true;
        }

        private static FileInfo GetFileInfo(string fileName)
        {
            long startPos = SECTOR_SIZE + 1;
            read.BaseStream.Seek(startPos, SeekOrigin.Begin);
            read.BaseStream.Seek(read.ReadInt64() * SECTOR_SIZE + 1, SeekOrigin.Begin);

            string nameFound = "";
            while (nameFound != fileName)
            {
                bool foundName = false;
                long curPos = startPos;
                while (!foundName)
                {
                    byte search = read.ReadByte();
                    curPos++;
                    if (curPos == startPos + SECTOR_SIZE - 9)
                    {
                        long nextSectorIndex = read.ReadInt64();
                        if (nextSectorIndex == 0)
                        {
                            return null;
                        }
                        else
                        {
                            startPos = nextSectorIndex * SECTOR_SIZE + 1;
                            curPos = startPos;
                            read.BaseStream.Seek(startPos, SeekOrigin.Begin);
                        }
                    }
                    else if (search != 0)
                    {
                        foundName = true;
                    }
                }

                read.BaseStream.Seek(-1, SeekOrigin.Current);
                nameFound = read.ReadString();
            }

            long namePos = read.BaseStream.Position - Encoding.UTF8.GetByteCount(nameFound) - 1;
            long dataSectorIndex = 0;
            long start = SECTOR_SIZE + 9;
            bool found = false;
            for (long i = start; i < start + SECTOR_SIZE - 17 && !found; i += FILE_INFO_SIZE)
            {
                read.BaseStream.Seek(i, SeekOrigin.Begin);
                dataSectorIndex = read.ReadInt64();
                if (dataSectorIndex != 0 && read.ReadInt64() == namePos)
                    found = true;
            }
            if (!found)
                return null;

            FileInfo result = new FileInfo();
            result.FileInfoPosition = read.BaseStream.Position - 16;
            result.DataSectorIndex = dataSectorIndex;
            result.Name = nameFound;
            result.ReadOnly = read.ReadBoolean();
            result.Created = VolumeMath.IntToDateTime(read.ReadUInt32());
            result.LastModified = VolumeMath.IntToDateTime(read.ReadUInt32());
            return result;
        }

        private static long FindOpenSectorIndex()
        {
            long result = 0;

            for (long i = 1; i < read.BaseStream.Length / SECTOR_SIZE && result == 0; i++)
            {
                read.BaseStream.Seek(i * SECTOR_SIZE, SeekOrigin.Begin);
                if (read.ReadByte() == 0)
                    result = i;
            }

            return result;
        }

        private static void ClearSector(long sectorIndex)
        {
            write.BaseStream.Seek(sectorIndex * SECTOR_SIZE + 1, SeekOrigin.Begin);
            for (int i = 0; i < SECTOR_SIZE - 9; i++)
                write.Write((byte)0);
        }

        enum SetAttribs { ReadOnly }
    }
}
