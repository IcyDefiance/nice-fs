﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFS
{
    class FileInfo
    {
        public long FileInfoPosition { get; set; }
        public long DataSectorIndex { get; set; }
        public string Name { get; set; }
        public bool ReadOnly { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
    }
}
