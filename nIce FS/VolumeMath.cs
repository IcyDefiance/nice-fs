﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFS
{
    static class VolumeMath
    {
        public static uint DateTimeToInt(DateTime dateTime)
        {
            return ((uint)dateTime.Month << 28) + ((uint)dateTime.Day << 23) + (((uint)dateTime.Year - 1970) << 16) +
                            ((uint)dateTime.Hour << 11) + ((uint)dateTime.Minute << 5) + ((uint)dateTime.Second / 2);
        }

        public static DateTime IntToDateTime(uint dateTime)
        {
            return new DateTime((int)((dateTime & 0x007F0000) >> 16), (int)((dateTime & 0xF0000000) >> 28), (int)((dateTime & 0x0F800000) >> 23),
                                           (int)((dateTime & 0x0000F800) >> 11), (int)((dateTime & 0x000007E0) >> 5), (int)(dateTime & 0x0000001F));
        }
    }
}
