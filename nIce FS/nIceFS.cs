﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace nIceFS
{
    static class nIceFS
    {
        public static bool Allocate(string volumePath, long size)
        {
            volumePath += ".nfs";
            if (File.Exists(volumePath))
                return false;
            else if (size < Volume.SECTOR_SIZE * Volume.MIN_SECTORS)
                return false;

            if (size % Volume.SECTOR_SIZE != 0)        //round size up to nearest sector size
                size += Volume.SECTOR_SIZE - size % Volume.SECTOR_SIZE;

            InitializeVolume(volumePath, size);

            return true;
        }

        public static bool Deallocate(string volumePath)
        {
            volumePath += ".nfs";

            File.Delete(volumePath);
            return true;
        }

        public static bool Truncate(string volumePath)
        {
            volumePath += ".nfs";

            long size = GetVolumeSize(volumePath);
            File.Delete(volumePath);
            InitializeVolume(volumePath, size);

            return true;
        }

        public static bool Dump(string volumePath)
        {
            volumePath += ".nfs";

            using (BinaryReader br = new BinaryReader(new FileStream(volumePath, FileMode.Open, FileAccess.Read)))
            {
                for (long i = 0; i < br.BaseStream.Length; i++)
                {
                    if (i % 16 == 0)
                        Console.WriteLine();
                    if (i % Volume.SECTOR_SIZE == 0)
                        Console.WriteLine();

                    Console.Write(Convert.ToString(br.ReadByte(), 16).PadLeft(2, '0') + " ");
                }
            }

            return true;
        }

        public static bool Mount(string volumePath)
        {
            volumePath += ".nfs";

            Volume.CommandLoop(volumePath);
            return true;
        }

        private static void InitializeVolume(string volumePath, long size)
        {
            using (BinaryWriter bw = new BinaryWriter(new FileStream(volumePath, FileMode.CreateNew)))
            {
                for (long i = 0; i < size; i++)
                    bw.Write((byte)0);

                bw.BaseStream.Seek(Volume.SECTOR_SIZE, SeekOrigin.Begin);
                bw.Write((byte)0xFF);
                bw.Write((long)2);
                bw.BaseStream.Seek(Volume.SECTOR_SIZE * 2, SeekOrigin.Begin);
                bw.Write((byte)0xFF);
            }
        }

        private static long GetVolumeSize(string volumePath)
        {
            return new System.IO.FileInfo(volumePath).Length;
        }
    }
}
