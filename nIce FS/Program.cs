﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFS
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("nIce FS v0.1a");
                Console.WriteLine("For a list of commands, type \"nIceFS help\"");
                return;
            }

            string command = args[0].ToLower();

            if (command == "help")
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Invalid syntax: help");
                    return;
                }

                Console.WriteLine("Available commands: allocate, deallocate, truncate, dump, mount");
            }
            else if (command == "allocate")
            {
                if (args.Length != 3)
                {
                    Console.WriteLine("Invalid syntax: allocate path sizeInBytes");
                    return;
                }

                long size;
                if (!long.TryParse(args[2], out size))
                {
                    Console.WriteLine("Invalid syntax: Size must be an integer.");
                    return;
                }

                bool result = nIceFS.Allocate(args[1], size);

                if (!result)
                {
                    Console.WriteLine("Error: Could not allocate volume. File may already exist, or size may be too small.");
                    return;
                }
            }
            else if (command == "deallocate")
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("Invalid syntax: deallocate path");
                    return;
                }

                bool result = nIceFS.Deallocate(args[1]);

                if (!result)
                {
                    Console.WriteLine("Error: Could not deallocate volume. Path does not refer to a nIce FS file.");
                    return;
                }
            }
            else if (command == "truncate")
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("Invalid syntax: truncate path");
                    return;
                }

                bool result = nIceFS.Truncate(args[1]);

                if (!result)
                {
                    Console.WriteLine("Error: Could not truncate volume. Path does not refer to a nIce FS file.");
                    return;
                }
            }
            else if (command == "dump")
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("Invalid syntax: dump path");
                    return;
                }

                bool result = nIceFS.Dump(args[1]);

                if (!result)
                {
                    Console.WriteLine("Error: Could not dump volume. Path does not refer to a nIce FS file.");
                    return;
                }
            }
            else if (command == "mount")
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("Invalid syntax: mount path");
                    return;
                }

                bool result = nIceFS.Mount(args[1]);

                if (!result)
                {
                    Console.WriteLine("Error: Could not mount volume. Path does not refer to a nIce FS file.");
                    return;
                }
            }
            else
            {
                Console.WriteLine("Invalid syntax: Command " + args[0] + " not recognized.");
                return;
            }
        }
    }
}
